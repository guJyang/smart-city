import * as THREE from 'three';
// 初始化渲染器
const renderer = new THREE.WebGLRenderer({
    // 抗锯齿
    antialias: true,
});

//设置渲染的尺寸大小
renderer.setSize(window.innerWidth, window.innerHeight);

// 开启阴影
renderer.shadowMap.enabled = true;

export default renderer;