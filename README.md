# 智慧城市
主要使用vue3+three.js进行项目开发，主要实现了以下功能：
- 城市展示
- 各类预警
- 飞线处理
- 精灵图
- 雷达
- 光线扫描

![效果图](./src/assets/banner.png)

## 修订
因为gitee 的pages服务关闭，我的博客失效，我的博客转移到了https://myblog-5g89ixpbbf1fbfad-1316695488.ap-shanghai.app.tcloudbase.com  (域名还在申请，这个是临时域名)

## 代码介绍
https://myblog-5g89ixpbbf1fbfad-1316695488.ap-shanghai.app.tcloudbase.com/2023/07/08/three-31/